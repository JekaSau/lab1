#!/bin/sh

current=$(pwd)
project=${PWD##*/}
build_dir=$current/"build"

if [ -n "$1" ] && [ "$1" = "debug" ]
then
	build_type=Debug
else
	build_type=Release
fi

echo "building $project as $build_type..."

rm -rf $build_dir
mkdir $build_dir
cd $build_dir

cmake -DCMAKE_BUILD_TYPE=$build_type $current

make

if [ -e $current/config ]
then
	cp $current/config/* .
fi

if [ -e $current/lib ]
then
	cp $current/lib/* .
fi

